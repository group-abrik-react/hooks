import React, { useState, useEffect } from 'react';

const UseEffect = () => {
    const [value, setValue] = useState(0);
    const [visible, setVisible] = useState(true);
    if (visible) {
        return (
            <div>
                <button
                    onClick={() => setValue((v) => v + 1)}>+</button>
                <button
                    onClick={() => setVisible(false)}> Hide</button>
                <HookCounter value={value} />
                <Notification />
            </div>
        )
    } else {
        return (
            <button
                onClick={() => setVisible(true)}> Show</button>
        )
    }

}

const HookCounter = ({ value }) => {
    useEffect(() => {
        console.log('mount');
        return () => console.log('clear')
    }, []);

    useEffect(() => console.log('update()'));

    return <p>x: {value}</p>
}

const Notification = () => {
    const [visible, setVisible] = useState(true);

    useEffect(() => {
        const timeout = setTimeout(()=> setVisible(false),5500);
        return ()=>clearTimeout(timeout);
    }, []);
    return <div> 
        {visible &&<p>Hello</p>}
        </div>
}

export default UseEffect;