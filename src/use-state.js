import React, { useState } from 'react';
import ReactDOM from 'react-dom';

const App = () => {
  return (
    <div>
      <HookSwitcher></HookSwitcher>
    </div>
  )
}

const HookSwitcher = () => {
  const [color, setColor] = useState('grey');
  const [fontSize, setFontSize] = useState(14);
  return (
    <div>
      <div>
        <button
          onClick={() => {
            setColor('grey')
            setFontSize(30)
          }}>
          Dark
        </button>

        <button
          onClick={() => {
            setColor('white')
            setFontSize(14)
          }}>
          Light
        </button>

        <button
          onClick={() => setFontSize((s) => s + 2)}>
          +
        </button>
      </div>
      <div style={{
        padding: '10px',
        backgroundColor: color,
        fontSize: `${fontSize}px`
      }}>

        HEllo

    </div>
    </div>
  )
}

ReactDOM.render(<App />,
  document.getElementById('root')
);