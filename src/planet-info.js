import React, { useCallback, useMemo, useEffect, useState } from 'react';


const getPlanet = (id) => {
    return (
        fetch(`https://reqres.in/api/users/${id}`)
            .then(res => res.json())
            .then(data => data)
    )
}
const useRequest = (request) => {
    const initislState = useMemo(() => ({
        data: null,
        loading: true,
        error: null
    }),[]);

    const [dataState, setDataState] = useState(initislState);

    useEffect(() => {
        setDataState(initislState);
        let canceled = false;
        request()
            .then(data => !canceled && setDataState({
                data,
                loading: false,
                error: null
            }))
            .catch(error =>  !canceled && setDataState({
                data: null,
                loading: false,
                error
            }));
        return () => canceled = true;
    }, [request,initislState]);
    return dataState;
}

const usePlanetInfo = (id) => {
    const request = useCallback(() => getPlanet(id), [id])
    return useRequest(request);
}

const PlanetInfo = ({ id }) => {
    const {data,loading,error} = usePlanetInfo(id);

    if(error)
    {
        return <div>Samething is wrong</div>
    }

    if(loading)
    {
        return <div>loading ...</div>
    }

    return (
        <div>Id: {id} - {data && data['data']['first_name']}</div>
    );
};

export default PlanetInfo;