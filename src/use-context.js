import React,{useContext} from 'react';

const MyContext = React.createContext();

const UseContext = ()=>{
    return (
        <MyContext.Provider value ="Hello World 123">
            <Child />
        </MyContext.Provider>
    )
};

const Child = ()=>{
    const value = useContext(MyContext);
    return(
        <div>
            <p>New: {value}</p>
            <MyContext.Consumer>
                {(value) =>{ return ( <p>{value}</p> )}}
            </MyContext.Consumer>
        </div>
    )
}

export default UseContext;