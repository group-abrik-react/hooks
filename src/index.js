import React,{useState,useEffect} from 'react';
import ReactDOM from 'react-dom';
import PlanetInfo from './planet-info';

const App =() => {
    const [value, setValue] = useState(1);
    const [visible, setVisible] = useState(true);
    if (visible) {
        return (
            <div>
                <button
                    onClick={() => setValue((v) => v + 1)}>+</button>
                <button
                    onClick={() => setVisible(false)}> Hide</button>
                <HookCounter value={value} />
                <PlanetInfo id={value}/>
            </div>
        )
    } else {
        return (
            <button
                onClick={() => setVisible(true)}> Show</button>
        )
    }    
}
const HookCounter = ({ value }) => {
    useEffect(() => {
        console.log('mount');
        return () => console.log('clear')
    }, []);

    useEffect(() => console.log('update()'));

    return <p>x: {value}</p>
}



ReactDOM.render(<App />,
    document.getElementById('root')
  );